+++
client_name = ""
date = "2016-04-27"
img = ""
img_preview = ""
summary = ""
tags = ["environmental sociology", "oil", "tourism", "governance"]
title = "3) The Oil-Tourism Interface"
external_link = ""

+++

I am a Co-Investigator on an exciting new environmental sociology project lead by [Mark Stoddart](http://www.mun.ca/soc/faculty/faculty-profiles/mark-c-j-stoddart.php) at Memorial University. The project, which is funded by an Insight Grant from the Social Science and Humanities Research Council of Canada (SSHRC), will compare oil development and tourism development in 5 North Atlantic regions. 

More generally, I collaborated with [Vanessa Schweizer](https://uwaterloo.ca/knowledge-integration/faculty-schweizer) and Mark Stoddart to host a workshop on "Challenges and Opportunities for Governance of Socio-Ecological Systems in Comparative Perspective" at the Balsillie School of International Affairs on April 21-22, 2016. The program is available [online](https://challengesopportunitiesenv.github.io) or [as a PDF](/pdfs/other/bsia_2016.pdf). We are currently looking into publishing a special issue. 


