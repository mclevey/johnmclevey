+++
client_name = ""
date = "2016-04-27"
img = ""
img_preview = ""
summary = ""
tags = ["think tanks", "public policy", "networks"]
title = "2) Think Tanks and Public Policy Research"
external_link = ""

+++

I wrote my dissertation at McMaster University on think tanks and the production of policy ideas. I have published articles from the dissertation project in *Social Studies of Science* and *Canadian Review of Sociology*. My dissertation research was funded in part by a Bombardier Canada Graduate Scholarship from the Social Science and Humanities Research Council of Canada (SSHRC) from 2010-2013. 

I am currently analyzing data for a new article on international think tank networks, and am about to return to a draft paper with Tom Medvetz that compares think tanks in the US and Canada. Finally, I am co-authoring a chapter with Ryan Deschamps on public policy formation and implementation for inclusion in a Sage Handbook of Political Sociology. 