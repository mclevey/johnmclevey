+++
client_name = ""
date = "2016-04-27"
img = ""
img_preview = ""
summary = ""
tags = ["computational social science", "networks", "agent based models"]
title = "5) Drug Agent: Modeling a Heroin Market"
external_link = ""

+++

I am collaborating with [Kirsten Robinson](https://uwaterloo.ca/waterloo-institute-for-social-innovation-and-resilience/people-profiles/kirsten-robinson) and [Owen Gallupe](https://uwaterloo.ca/sociology-and-legal-studies/people-profiles/owen-gallupe) on an agent based model of a heroin market. Our model, which is still in development, tests the effectiveness of strategies that dealers use to evade arrest. In doing so, it speaks to broader questions about *resilience* in social networks and complex systems more broadly. The model is based, in part, on crime data from Australia. 