+++
client_name = ""
date = "2016-04-27"
img = ""
img_preview = ""
summary = ""
tags = []
title = "4) Robotics and 21st Century Education"
external_link = ""

+++

I am in the *very* early stages of a project on the introduction of robotics into elementary and junior high school curriculum in Ontario. This research is funded by the [Council of Ontario Directors of Education](http://www.ontariodirectors.ca), or CODE. [Janice Aurini](https://uwaterloo.ca/sociology-and-legal-studies/people-profiles/janice-aurini) is the project lead. My role is primarily as a survey methodologist, and my colleague [Rob Gorbet](https://uwaterloo.ca/knowledge-integration/faculty-gorbet) is involved as an expert in robotics. We will all be co-authoring papers from this project with graduate students from Sociology & Legal Studies at the University of Waterloo. 