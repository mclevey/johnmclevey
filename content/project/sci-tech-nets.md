+++
client_name = ""
date = "2016-04-27"
img = ""
img_preview = ""
summary = ""
tags = ["social networks", "computational social science", "metaknowledge", "gitnet", "FLOSS"]
title = "1) Structure and Evolution of Scientific and Technical Networks"
external_link = ""

+++

I am working on a series of related papers with colleagues that are all *broadly* about the structure and dynamics of scientific and technical networks. First, I am working with [Allyson Stokes](http://aejstokes.github.io/) on papers about structural changes in the intellectual networks of Canadian sociology from 1966-2015. Second, I am working with [Katie Plaisance](https://uwaterloo.ca/knowledge-integration/faculty-plaisance) and some of my students on modeling knowledge diffusion processes using the case of philosophy of science. Finally, I am in the early stages of developing a new series of papers on the co-evolution of biomedical research and development networks with open science practices. [Martin Cooke](https://uwaterloo.ca/sociology-and-legal-studies/people-profiles/martin-cooke) and [Owen Gallupe](https://uwaterloo.ca/sociology-and-legal-studies/people-profiles/owen-gallupe) are collaborators on a SSHRC grant currently under review. These projects have lead to me working with my former student [Reid McIlroy-Young](http://reidmcy.com/) on a Python package called [metaknowledge](http://networkslab.org/metaknowledge/) for simplifying quantitative and social networks research on science. We are currently co-authoring an article that introduces the package to scientometric, science policy, and social networks researchers. 

I am also co-authoring a series of papers with my [NetLab](http://networkslab.org) research assistants on collaboration networks in free / libre / open source development. Most of these papers are using statistical models for network analysis, such as exponential random graph models (ERGMs). We are co-developing a Python package called [gitnet](http://networkslab.org/gitnet/), which will mine data from version control systems like git. Later releases will include support for mining data from developer mailing lists. 