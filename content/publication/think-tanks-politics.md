+++
abstract = "The relationships between think tanks and their funders are central to theory and public discourse about the politics of policy knowledge, yet very little research systematically examines these relationships across cases. This article evaluates elite, pluralist, and field theories by analyzing original data on funding and politics for 30 think tanks from 2000 to 2011 with comparative and relational methods. I find that foreign donations help support some conservative think tanks, but that it is a small amount of money relative to other funding sources. Domestically, think tank funding is structured by an opposition between donor-funded conservatives and state-funded centrists. Since 2005, the cluster of conservative think tanks funded by private donors has become tighter, while the cluster of think tanks supported by the state has become looser and more reliant on self-generated revenue and interest and investments. These findings cast doubt on predictions derived from elite and pluralist theories, and offer some support for field theory."
authors = ["John McLevey"]
date = "2014-01-01"
math = true
publication = "In *Canadian Review of Sociology*"
title = "Think Tanks, Funding, and the Politics of Policy Knowledge in Canada"
url_code = ""
url_dataset = ""
url_supplement = ""
url_image = ""
url_pdf = "http://bit.ly/mclevey_2014"
url_project = "/project/think-tanks-pol-soc/"
url_slides = ""
url_video = ""

+++

This article was funded in part by ... 
