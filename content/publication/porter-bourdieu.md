+++
abstract = "How has English Canadian sociology changed from 1966-2014? Has it become more intellectually fragmented or cohesive over time? We answer these questions by analyzing co-citation networks extracted from 7,141 sociology articles published in 169 journals. We show how the most central early specialties developed largely in response to John Porter’s The Vertical Mosaic. In later decades, the discipline diversified, fragmented, and then reorganized around a new set of specialties knit together by the work of Pierre Bourdieu. The discipline was most intellectually fragmented in periods where multiple specialties were emerging or declining concurrently (i.e. 1975-1985 and 1995-2004), and was more structurally cohesive from 2005-2014 than in any previous period."
authors = ["Allyson Stokes", "John McLevey"]
date = "2016-08-01"
math = true
publication = "In *Canadian Review of Sociology*"
title = "From Porter to Bourdieu: The Evolving Specialty Structure of English Canadian Sociology, 1966-2014."
url_code = "https://github.com/mclevey/porter-to-bourdieu-code/"
url_dataset = ""
url_image = ""
url_pdf = "http://bit.ly/stokes_mclevey_2016"
url_project = "/project/sci-tech-nets/"
url_slides = ""
url_video = ""

+++

# Online Supplement: List of 169 Journals in the Analysis

Acta Sociologica    
Actes de la recherche en sciences sociales    
Administrative Science Quarterly    
Afro-American Studies    
Agriculture and Human Values    
American Journal of Economics and Sociology    
American Journal of Sociology    
American Sociological Review    
American Sociologist    
Annals of Phenomenological Sociology    
Annals of Tourism Research    
Annual Review of Law and Social Science    
Annual Review of Sociology    
Applied Social Studies    
Armed Forces & Society    
Australian and New Zealand Journal of Sociology    
Blue Ribbon Papers: Interactionism the Emerging Landscape    
Body & Society    
British Journal of Sociology    
British Journal of Sociology of Education    
Cahiers internationaux de sociologie    
Canadian Journal of Sociology (also searched under French name)    
Canadian Review of Sociology (and Anthropology) (also searched under French name)    
Canadian Studies in Population    
City & Community    
Comparative Sociology    
Comparative Studies in Society and History    
Contemporary Sociology - A Journal of Reviews    
Contributions to Indian Sociology    
Crime Media Culture    
Criminology    
Critical Sociology    
Cultural Sociology    
Current Sociology    
Current Sociology    
Demography    
Deviance et societe    
Deviant Behavior    
Discourse & Society    
Du Bois Review - Social Science Research on Race    
Economic and Social Review    
Economy and Society    
Ethnic and Racial Studies    
Ethnography    
European Journal of Social Theory    
European Societies    
European Sociological Review    
Food Culture & Society    
Future of Children    
Gender & Society    
Global Networks - A Journal of Transnational Affairs    
Health Sociology Review    
Human Context    
Human Ecology    
Human Ecology Review    
Human Nature    
Human Studies    
Indian Sociological Bulletin    
Information Communication & Society    
Innovation - The European Journal of Social Science Research    
International Journal of Comparative Sociology    
International Journal of Contemporary Sociology    
International Journal of Intercultural Relations    
International Journal of Sociology of the Family    
International Journal of the Sociology of Law    
International Political Sociology    
International Review For the Sociology of Sport    
International Review of Modern Sociology    
International Sociology    
International Review of Sociology    
Jewish Journal of Sociology    
Journal For the Scientific Study of Religion    
Journal of Consumer Culture    
Journal of Contemporary Ethnography    
Journal of Family Issues    
Journal of Historical Sociology    
Journal of Human Relations    
Journal of Law and Society    
Journal of Leisure Research    
Journal of Marriage and Family    
Journal of Marriage and the Family    
Journal of Mathematical Sociology    
Journal of Political & Military Sociology    
Journal of Sociology    
Journal of Sociology and Social Welfare    
Journal of Sport & Social Issues    
Journal of the History of Sexuality    
Journal of Voluntary Action Research    
Language in Society    
Law & Society Review    
Leisure Sciences    
Loisir & societe - Society and Leisure    
Mass Emergencies    
Media Culture & Society    
Men and Masculinities    
Mid-American Review of Sociology    
Mobilization    
Nations and Nationalism    
Netherlands Journal of Social Sciences    
Pacific Sociological Review    
Play & Culture    
Poetics    
Politics & Society    
Population and Development Review    
Public Opinion Quarterly    
Qualitative Research    
Qualitative Sociology    
Race & Class    
Radical Interactionism on the Rise    
Rationality and Society    
Research in Social Stratification and Mobility    
Review of Religious Research    
Revue De L Institut De Sociologie    
Revue Francaise De Sociologie    
Rural Sociology    
Sage Professional Paper in Contemporary Political Sociology Series    
Science Technology & Human Values    
Scientometrics    
Scottish Journal of Sociology    
Sexualities    
Social Compass    
Social Forces    
Social Indicators Research    
Social Movement Studies    
Social Networks    
Social Problems    
Social Psychology Quarterly    
Social Science Quarterly    
Social Science Research    
Social Studies of Science    
Social Studies - Irish Journal of Sociology    
Society & Natural Resources    
Society and Mental Health    
Socio-Economic Review    
Sociologia Ruralis    
Sociological Analysis    
Sociological Analysis & Theory    
Sociological Bulletin    
Sociological Focus    
Sociological Forum    
Sociological Inquiry    
Sociological Methodology    
Sociological Methods & Research    
Sociological Perspectives    
Sociological Quarterly    
Sociological Research Online    
Sociological Review    
Sociological Spectrum    
Sociological Symposium    
Sociological Theory    
Sociological Theory and Methods    
Sociologie du travail    
Sociologie et societes    
Sociology and Social Research    
Sociology of Education    
Sociology of Health & Illness    
Sociology of Religion    
Sociology of Sport Journal    
Sociology of Work and Occupations    
Sociology - The Journal of the British Sociological Association    
Southwestern Social Science Quarterly    
Studies in Symbolic Interaction    
Symbolic Interaction    
Teaching Sociology    
Theory and Society    
Theory Culture & Society    
Work and Occupations    
Work Employment and Society    
Youth & Society    
