+++
abstract = "Research on scientific, social scientific, and technical knowledge is increasingly focused on changes in institutionalized fields, such as the commercialization of university-based knowledge. Much less is known about how organizations produce and promote knowledge in the ‘thick boundaries’ between fields. In this article, I draw on 53 semi-structured interviews with Canadian think-tank executives, researchers, research fellows, and communication officers to understand how think- tank knowledge work is linked to the liminal spaces between institutionalized fields. First, although think-tank knowledge work has a broadly utilitarian epistemic culture, there are important differences between organizations that see intellectual simplicity and political consistency as the most important marker of credibility, versus those that emphasize inconsistency. A second major difference is between think tanks that argue for the separation of research and communication strategies and those that conflate them from beginning to end, arguably subordinating research to demands from more powerful fields. Finally, think tanks display different degrees of instrumentalism toward the public sphere, with some seeking publicity as an end in itself and others using it as a means to influence elite or public opinion. Together, we can see these differences as responses to diverging principles of legitimacy."
authors = ["John McLevey"]
date = "2015-01-01"
math = true
publication = "In *Social Studies of Science*"
title = "Understanding Policy Research in Liminal Spaces: Think Tank Responses to Diverging Principles of Legitimacy"
url_code = ""
url_dataset = ""
url_supplement = ""
url_image = ""
url_pdf = "http://bit.ly/mclevey_2015"
url_project = "/project/think-tanks-pol-soc/"
url_slides = ""
url_video = ""

+++

This article was funded in part by ... 
