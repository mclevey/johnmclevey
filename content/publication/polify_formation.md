+++
abstract = ""
authors = ["John McLevey", "Ryan Deschamps"]
editors = ["Steven Turner", "William Outhwaite"]
date = "2016-12-01"
math = true
publication = "In *The SAGE Handbook of Political Sociology*"
title = "Public Policy Formation and Implementation"
url_code = ""
url_dataset = ""
url_image = ""
url_pdf = ""
url_project = "/project/think-tanks-pol-soc/"
url_slides = ""
url_video = ""

+++
