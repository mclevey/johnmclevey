+++
abstract = "*metaknowledge* is a full-featured Python package for computational research in information science, network analysis, and science of science. It is optimized to scale efficiently for analyzing very large datasets, and is designed to integrate well with reproducible and open research workflows. It currently accepts raw data from the Web of Science, Scopus, PubMed, ProQuest Dissertations & Theses, and select funding agencies. It processes these raw data inputs and outputs a variety of datasets for quantitative analysis, including time series methods, Standard and Multi Reference Publication Year Spectroscopy, computational text analysis (e.g. topic modeling, burst analysis), and network analysis (including multi-mode, multi-level, and longitudinal networks). This article motivates the use of *metaknowledge* and explains its design and core functionality."
authors = ["John McLevey", "Reid McIlroy-Young"]
date = "2017-01-01"
math = true
publication = "In *Journal of Informetrics*"
title = "Introducing *metaknowledge*: Software for Information Science, Network Analysis, and Science of Science"
url_code = "https://github.com/mclevey/metaknowledge_article_supplement"
url_dataset = "https://github.com/mclevey/metaknowledge_article_supplement"
url_supplement = "https://github.com/mclevey/metaknowledge_article_supplement"
url_image = ""
url_pdf = "http://bit.ly/metaknowledge"
url_project = "project/sci-tech-nets/"
url_slides = ""
url_video = ""

+++

Thanks to Jillian Anderson, Steven McColl, Alexander Graham, Amelia Howard, and Pierson Browne for comments on an earlier version of this manuscript. Jillian Anderson developed the Javascript library *mkD3* that enables interactive visualizations of *metaknowledge* datasets. She is a *metaknowledge* developer on releases >3.1.

*metaknowledge* is funded in part by a Social Science and Humanities Research Council of Canada grant and an Early Researcher Award from the Ministry of Research and Innovation in Ontario, both awarded to Dr. John McLevey.