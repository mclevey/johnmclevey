+++
abstract = ""
authors = ["John McLevey", "Allyson Stokes", "Amelia Howard"]
date = "2016-12-01"
math = true
publication = "In *The Oxford Handbook of Pierre Bourdieu*"
title = "Pierre Bourdieu's Uneven Influence on Anglophone Canadian Sociology."
url_code = ""
url_dataset = ""
url_image = ""
url_pdf = ""
url_project = "/project/sci-tech-nets/"
url_slides = ""
url_video = ""

+++
