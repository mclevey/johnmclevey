+++
date = "2016-04-20T00:00:00"
draft = false
title = "about"
section_id = 0
weight = 0
+++

## Biography

I'm an Assistant Professor in the Department of Knowledge Integration at the University of Waterloo, and am cross-appointed to Sociology & Legal Studies and the School of Environment, Resources, & Sustainability. I'm also an Affiliated Researcher at the Waterloo Institute for Complexity and Innovation, a Policy Fellow at the Balsillie School of International Affairs, and a board member for the University of Waterloo Survey Research Center. You can [download my full CV here](/pdfs/cv_mclevey.pdf).

In addition to writing articles, I am co-developing research software with student members of my research team. Our work is funded in part by grants from the Social Science and Humanities Research Council of Canada (SSHRC) and an [Early Researcher Award](https://www.ontario.ca/page/early-researcher-awards) from the Ministry of Research and Innovation in Ontario. You can learn more about [metaknowledge](http://networkslab.org/metaknowledge/) and [gitnet](http://networkslab.org/gitnet) on my [NetLab](http://networkslab.org/) website. I recently published an article about _metaknowledge_ (with Reid McIlroy-Young) in _Journal of Informetrics_. You can read it [here](http://www.sciencedirect.com/science/article/pii/S1751157716302000).

If you are a student, you can book a meeting with me using [mclevey.youcanbook.me](https://mclevey.youcanbook.me).
