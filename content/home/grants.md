+++
date = "2016-04-20T00:00:00"
draft = false
title = "Grants"
section_id = 10
weight = 10
+++

### Research Grants

*Total Funded Research Grants since 2013: $563,729*   

- 2016-2018 John McLevey (PI), Owen Gallupe (Collaborator) and Martin Cooke (Collaborator), [Social Science and Humanities Research Council of Canada (**SSHRC**)](http://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_development_grants-su
bventions_de_developpement_savoir-eng.aspx), Insight Development Grant, “Remaking the Boundaries of Open and Proprietary Science: A Longitudinal Study of Biomedical Research and Development Networks in Canada” $67,790.
- 2016-2018 Katie Plaisance (PI) and John McLevey (Co-I), [Social Science and Humanities Research Council of Canada (**SSHRC**)](http://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_development_grants-su
bventions_de_developpement_savoir-eng.aspx), Insight Development Grant, "Increasing the Impact of Philosophy of Science in Scientific Domains", $59,302
- 2016-2020 John McLevey (PI), Ontario [Early Researcher Award](https://www.ontario.ca/document/ontario-research-fund-early-researcher-awards-program-guidelines), Ministry of Research and Innovation, "Information and Idea Diffusion in an Open Source Collaboration Network," $150,000.  
- 2015-2016 John McLevey (PI) and Vanessa Schweizer (Co-I), Basillie School of International Affairs, Major Workshops Grant, "Challenges and Opportunities for Governance of Socio-Ecological Systems in Comparative Perspective," $8,400.
- 2015-2020 Mark Stoddart (PI) and John McLevey (Co-I), [Social Science and Humanities Research Council of Canada (**SSHRC**)](http://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_development_grants-su
bventions_de_developpement_savoir-eng.aspx), Insight Grant, "The Oil-Tourism Interface and Social-Ecological Change in the North Atlantic," $188,423.
- 2014-2016 John McLevey (PI), [Social Science and Humanities Research Council of Canada (**SSHRC**), Insight Development Grant](http://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_development_grants-su
bventions_de_developpement_savoir-eng.aspx), "Collaborative Design in Online Networks," $74,814.
- 2013 University of Waterloo, Faculty Research Startup Grant, $15,000.

### Grant Applications Under Review

- Under Review. David Tindall (PI), John McLevey (Co-I), Mark Stoddart (Co-I). Collaborators: Andrew Jorgenson, Philippe Le Billon, Mario Diani, Jennifer Earl, Don Grant, Philip Leifeld, Dana R Fisher, Moses Boudourides. [Social Science and Humanities Research Council of Canada (**SSHRC**), Insight Grant](http://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_grants-subventions_savoir-eng.aspx) Application. "Making sense of climate action: Understanding social mobilization to curb anthropogenic climate change through advances in social network analysis."

### Teaching Grants

- 2015-2016 John McLevey (PI), LITE Seed Grant, Center for Teaching Excellence, University of Waterloo, "A Data-Driven and Interactive Approach to Enhancing Student Learning about Disciplinary and Interdisciplinary Knowledge Production," $4,765.20.

### Consulting & Research Contracts

- Janice Aurini (PI), John McLevey (PI), and Rob Gorbet (Co-I). 2016. "Determining the impact of classroom-integrated robotics education on acquisition and development of 21st Century competencies." Research contract with the Council of Ontario Directors of Education (CODE). Research Collaborators: Dr. Allyson Stokes, Dr. Scott Davies.
