+++
date = "2016-04-20T00:00:00"
draft = false
title = "teaching"
section_id = 10
weight = 10
+++

### Planned Courses

Knowledge Integration is a small department. We have to plan things fairly far in advance to cover program requirements, juggle sabbaticals, etc. As of right now, I have agreed to the sequence of courses listed below. *This is just a plan. It might change*. 

I will be adding some teaching materials (e.g. syllabi, slides, assignment guidelines) here as I develop them. This information will also be available to students on a private LEARN website. 

In addition to the list below, I am developing courses on Research Synthesis Methods, systematic reviews, and bibliometrics.

**Fall 2016**

- INTEG 120: Disciplines & Integrative Practices 
    - [syllabus (`.pdf`)](pdfs/teaching/120_disciplines.pdf)
    <!-- - [slides (`.html`)](integ120/) -->
    <!-- - [systems thinking paper (`.html`) grading rubric](http://www.johnmclevey.com/systems-thinking-paper/) -->
    <!-- - [knowledge networks paper (`.html`) grading rubric](http://www.johnmclevey.com/knowledge-networks-paper/) -->
    <!-- - [course calendar file (`.ics`)](http://www.johnmclevey.com/integ120/downloads/INTEG120.ics) -->
- INTEG 375: Research Methods & Design 
    - [syllabus (`.pdf`)](pdfs/teaching/375_methods.pdf)
    <!-- - [slides (`.html`)](research-methods/) -->
    <!-- - [code for R lessons](r_lessons/) (`.html`) -->
    <!-- - [research proposal (`.html`) grading rubric](http://www.johnmclevey.com/research-proposal-rubric/) -->
    <!-- - [empirical research paper (`.html`) grading rubric](http://www.johnmclevey.com/research-paper-rubric/) -->
    <!-- - [course calendar file (`.ics`)](http://www.johnmclevey.com/research-methods/downloads/INTEG375.ics) -->

**Winter 2017**

- **No teaching, I will be on sabbatical**

**Fall 2017**

- INTEG 120: Disciplines & Integrative Practices
- INTEG 420A: Senior Thesis Seminar 

**Winter 2018**

- INTEG 340: Research Methods and Design
- INTEG 420B: Senior Thesis Seminar
- *INTEG elective*, most likely on science & policymaking, public understanding of science, computational social science, or data visualization

**Fall 2018**

- INTEG 120: Disciplines & Integrative Practices
- INTEG 420A: Senior Thesis Seminar (co-taught with Vanessa Schweizer)
- *INTEG elective* on policy formation and decision making with a substantive focus on environmental policy (co-taught with Vanessa Schweizer)

### Past Courses

- Research Methods & Design
- Data Visualization -- [Winter 2016 PDF syllabus](/pdfs/teaching/375_visualization.pdf)
- Disciplines and Integrative Practices
- Open Science / Computational Social Science -- [Fall 2015 PDF syllabus](/pdfs/teaching/475_open.pdf)  
- Sociology of Science -- [Winter 2015 PDF syllabus](/pdfs/teaching/312_science.pdf)
- Creativity & Innovation -- this course is currently being taught by Vanessa Schweizer. 

I have also occasionally taught undergraduate and graduate reading courses in social network analysis, computational social science, sociology of science, organizations, and research methods. I only teach these courses under very specific circumstances.

### Student Supervision

PhD completed  

- Michael Clarke (dissertation committee member)   

PhD in progress   

- Amelia Howard (dissertation co-supervisor) 
- Rod Missaghian (dissertation committee member)
- Noorin Manji (dissertation committee member)
- Yixi Yang (dissertation committee member, supervisor Mark Stoddart at Memorial University)  
- Pierson Browne (dissertation committee member) 

MA in progress:    

- Alexander (Sasha) Graham (thesis supervisor)
- Junyi (Jill) Wang (reader, supervisor Derek Robinson)
- Nicholas Brandon, MD (thesis committee member, School of Public Health and Health Systems, Faculty of Applied Health Sciences, supervisor Shannon Majowicz)

Undergraduate honors in progress and completed:   

- Jillian Anderson (in progress 2016-17)
- Julia Yaroshinsky (completed 2016)
- Tiffany Lin (completed 2015)
- Benjamin Carr (completed 2014)
- Chelsea Mills (completed 2013)
- Christina Minji Chung (completed 2013)   

### Research Assistants 

**Current**: Jillian Anderson (UG), Joel Becker (UG), Steve McColl (UG), Pierson Browne (PhD), Brittney Etmanski (PhD), Sasha Graham (MA), Yixi Yang (PhD)    

**Past**: Reid McIlroy-Young (UG), Amelia Howard (PhD), Tiffany Lin (UG), Evaleen Hellinga (UG)    

For information about my research assistants, please see the [NetLab website](http://networkslab.org/).

### Teaching Assistants 

Bronwyn McIlroy-Young (INTEG 120 -- 2014, 2016), Samantha Afonso (INTEG 120 & INTEG 375 Research Design and Methods -- 2016), Rebecca Little (INTEG 120 -- 2015), Geoff Evamy-Hill (INTEG 120 -- 2013)