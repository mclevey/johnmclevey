#!/bin/bash

echo -e "\033[0;32mBuilding the site...\033[0m"

hugo

echo -e "\033[0;32mMoving things to the github repo\033[0m"

cp -rf public/ ~/Documents/websites/john/live/mclevey.github.io/

cd ~/Documents/websites/john/live/mclevey.github.io/

echo -e "\033[0;32mDeploying\033[0m"

# Add changes to git.
git add -A

# Commit changes.
msg="rebuilding site `date`"
if [ $# -eq 1 ]
  then msg="$1"
fi
git commit -m "$msg"

git push